package org.bakhtiargalib.orderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ProductResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    private String title;

    private String description;

    private double unitPrice;

    private int stockAmount;
}

