package org.bakhtiargalib.orderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bakhtiargalib.orderservice.entity.Order;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class OrderRequestDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private List<OrderLineRequestDto> orderLines;

    public Order toOrder() {
        return Order.builder()
                .status(Order.Status.DRAFT)
                .orderLines(orderLines.stream()
                        .map(op -> op.toOrderLine())
                        .collect(Collectors.toList()))
                .build();
    }
}