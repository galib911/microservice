package org.bakhtiargalib.orderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bakhtiargalib.orderservice.entity.OrderLine;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrderLineRequestDto {

    @NotNull
    private Long productId;

    @Min(1)
    @NotNull
    private Integer quantity;

    public OrderLine toOrderLine() {
        return OrderLine.builder()
                .productId(productId)
                .quantity(quantity)
                .build();
    }
}
