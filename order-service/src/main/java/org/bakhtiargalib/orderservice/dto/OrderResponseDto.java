package org.bakhtiargalib.orderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bakhtiargalib.orderservice.entity.Order;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class OrderResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    private List<OrderLineResponseDto> orderLines;

    private Order.Status status;

    private double vat;

    private double discount;

    private double total;

    private double billableAmount;
}