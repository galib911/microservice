package org.bakhtiargalib.orderservice.service;

import org.bakhtiargalib.orderservice.ResourceNotFoundException;
import org.bakhtiargalib.orderservice.dto.OrderLineRequestDto;
import org.bakhtiargalib.orderservice.dto.OrderLineResponseDto;
import org.bakhtiargalib.orderservice.dto.OrderResponseDto;
import org.bakhtiargalib.orderservice.dto.ProductResponseDto;
import org.bakhtiargalib.orderservice.entity.Order;
import org.bakhtiargalib.orderservice.entity.OrderLine;
import org.bakhtiargalib.orderservice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    public OrderResponseDto getOrderResponse(Order order) {

        OrderResponseDto orderResponse = OrderResponseDto.builder()
                .id(order.getId())
                .orderLines(
                        order.getOrderLines()
                                .stream()
                                .map(ol -> {
                                    ProductResponseDto productResponse = getProductResponse(ol.getProductId());

                                    return new OrderLineResponseDto(
                                            ol.getId(),
                                            productResponse.getTitle(),
                                            productResponse.getUnitPrice(),
                                            ol.getQuantity(),
                                            productResponse.getUnitPrice() * ol.getQuantity()
                                    );
                                })
                                .collect(Collectors.toList())
                )
                .status(order.getStatus()).build();

        orderResponse.setTotal(
                orderResponse.getOrderLines()
                        .stream()
                        .mapToDouble(l -> l.getSubtotal())
                        .sum()
        );

        orderResponse.setVat(orderResponse.getTotal() * 0.15);
        orderResponse.setDiscount(orderResponse.getTotal() * 0.05);
        orderResponse.setBillableAmount(orderResponse.getTotal() + orderResponse.getVat() - orderResponse.getDiscount());

        return orderResponse;
    }

    public ProductResponseDto getProductResponse(long productId) {

        return circuitBreakerFactory.create("getProductResponseCb").run(
                () -> restTemplate.getForObject("http://inventory-service/products/" + productId,
                        ProductResponseDto.class)
        );
    }

    public List<Order> list(int index,
                            int listSize,
                            Sort sort) {

        return orderRepository.findAll(PageRequest.of(index, listSize, sort)).toList();
    }

    public Order getOne(long id) {

        return orderRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Order with Id " + id + " not found"));
    }

    @Transactional
    public Order save(Order order) {

        return orderRepository.save(order);
    }

    @Transactional
    public Order cancel(long id) {
        Order order = getOne(id);

        order.setStatus(Order.Status.CANCELED);

        return order;
    }

    @Transactional
    public Order complete(long id) {
        Order order = getOne(id);

        updateInventory(order);

        order.setStatus(Order.Status.COMPLETED);

        return order;
    }

    private void updateInventory(Order order) {

        for (OrderLine orderLine : order.getOrderLines()) {
            circuitBreakerFactory.create("removeStockCb").run(() ->
                    restTemplate.exchange("http://inventory-service/products/removeStock",
                            HttpMethod.PUT,
                            new HttpEntity<>(new OrderLineRequestDto(orderLine.getProductId(), orderLine.getQuantity())),
                            ProductResponseDto.class
                    )
            );
        }
    }
}
