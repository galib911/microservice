package org.bakhtiargalib.orderservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ErrorResponseDto implements Serializable {

    private String code;

    private String message;

    private List<String> globalErrors;

    private Map<String, String> fieldErrors;
}
