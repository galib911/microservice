package org.bakhtiargalib.orderservice.validator;

import org.bakhtiargalib.orderservice.dto.OrderLineRequestDto;
import org.bakhtiargalib.orderservice.dto.OrderRequestDto;
import org.bakhtiargalib.orderservice.dto.ProductResponseDto;
import org.bakhtiargalib.orderservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

@Component
public class OrderValidator {

    @Autowired
    private OrderService orderService;

    public void validateOrderRequest(OrderRequestDto orderRequest, BindingResult result) {

        for (OrderLineRequestDto orderLine : orderRequest.getOrderLines()) {
            validateOrderLineRequest(orderLine, (BindingResult) result);
        }
    }

    public void validateOrderLineRequest(OrderLineRequestDto orderLineRequestDto, BindingResult result) {
        ProductResponseDto productResponse = orderService.getProductResponse(orderLineRequestDto.getProductId());

        if (productResponse == null) {
            result.rejectValue("productId",
                    "error.product.does.not.exist",
                    new Object[]{productResponse.getId()},
                    null);

            return;
        }

        if (productResponse.getStockAmount() < orderLineRequestDto.getQuantity()) {
            result.rejectValue("productId",
                    "error.product.stockout",
                    new Object[]{productResponse.getId()},
                    null);

            return;
        }
    }
}
