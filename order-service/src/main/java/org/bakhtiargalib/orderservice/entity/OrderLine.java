package org.bakhtiargalib.orderservice.entity;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@Entity
public class OrderLine extends Persistent {

    @Id
    @GeneratedValue(generator = "orderLineIdGenerator", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "orderLineIdGenerator", sequenceName = "order_line_seq", allocationSize = 1)
    private long id;

    private long productId;

    private int quantity;
}
