package org.bakhtiargalib.orderservice.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@Table(name = "ORDERS")
@Entity
public class Order extends Persistent {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "orderIdGenerator", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "orderIdGenerator", sequenceName = "order_seq", allocationSize = 1)
    private long id;

    @JoinColumn(name = "order_id")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OrderLine> orderLines;

    @Enumerated(EnumType.STRING)
    private Status status;

    public Order() {
        orderLines = new ArrayList<>();
        status = Status.DRAFT;
    }

    @AllArgsConstructor
    @Getter
    public enum Status {
        DRAFT("Draft"),
        //CONFIRMED("Confirmed"),
        //PAID("Paid"),
        //PROCESSING("Processing"),
        COMPLETED("Completed"),
        CANCELED("Canceled");

        private String label;
    }
}
