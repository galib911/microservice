package org.bakhtiargalib.orderservice.controller;

import org.bakhtiargalib.orderservice.ErrorResponseDto;
import org.bakhtiargalib.orderservice.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.bakhtiargalib.orderservice.ApiResponseUtils.errorResponse;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException exception) {

        return errorResponse(
                new ErrorResponseDto("404", exception.getMessage(), null, null),
                HttpStatus.NOT_FOUND
        );
    }

    /*@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return validationErrorResponse(ex.getBindingResult());
    }*/
}
