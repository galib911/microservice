package org.bakhtiargalib.orderservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.bakhtiargalib.orderservice.dto.OrderLineRequestDto;
import org.bakhtiargalib.orderservice.dto.OrderRequestDto;
import org.bakhtiargalib.orderservice.dto.OrderResponseDto;
import org.bakhtiargalib.orderservice.entity.Order;
import org.bakhtiargalib.orderservice.service.OrderService;
import org.bakhtiargalib.orderservice.validator.OrderValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.bakhtiargalib.orderservice.ApiResponseUtils.successResponse;
import static org.bakhtiargalib.orderservice.ApiResponseUtils.validationErrorResponse;

@Slf4j
@RestController
@RequestMapping("/orders")
public class OrderController {

    private static final String DEFAULT_MAX_ITEMS_PER_PAGE = "10";
    private static final String DEFAULT_ORDER_BY_PROPERTY = "updated";
    private static final String DEFAULT_ORDER_DIRECTION = "DESC";

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderValidator orderValidator;

    @GetMapping
    public ResponseEntity<List<OrderResponseDto>> list(@RequestParam(defaultValue = "0") int index,
                                                       @RequestParam(defaultValue = DEFAULT_MAX_ITEMS_PER_PAGE) int listSize,
                                                       @RequestParam(defaultValue = DEFAULT_ORDER_BY_PROPERTY) String orderBy,
                                                       @RequestParam(defaultValue = DEFAULT_ORDER_DIRECTION) Sort.Direction orderDirection) {

        log.info("[OrderController:List] index = {}, " +
                        "listSize = {}, " +
                        "orderBy = {}, " +
                        "orderDirection = {}",
                index, listSize, orderBy, orderDirection);

        Sort sort = Sort.by(orderDirection, orderBy);

        return successResponse(
                orderService.list(index, listSize, sort)
                        .stream()
                        .map(o -> orderService.getOrderResponse(o))
                        .collect(Collectors.toList())
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderResponseDto> getOne(@PathVariable long id) {

        return successResponse(
                orderService.getOrderResponse(orderService.getOne(id))
        );
    }

    @PostMapping
    public ResponseEntity<?> save(@Valid @RequestBody OrderRequestDto orderRequestDto,
                                  BindingResult result) {

        orderValidator.validateOrderRequest(orderRequestDto, result);

        if (result.hasErrors()) {

            return validationErrorResponse(result);
        }

        return successResponse(
                orderService.getOrderResponse(orderService.save(orderRequestDto.toOrder()))
        );
    }

    @PutMapping("/complete/{id}")
    public ResponseEntity<?> complete(@PathVariable long id) {

        return successResponse(
                orderService.getOrderResponse(orderService.complete(id))
        );
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> cancel(@PathVariable long id) {

        return successResponse(
                orderService.getOrderResponse(orderService.cancel(id))
        );
    }

    @PostMapping(value = {"/orderLines", "/{orderId}/orderLines"})
    public ResponseEntity<?> addOrderLine(@Valid @RequestBody OrderLineRequestDto orderLineRequestDto,
                                          BindingResult result,
                                          @PathVariable(required = false) Long orderId) {

        orderValidator.validateOrderLineRequest(orderLineRequestDto, result);

        if (result.hasErrors()) {

            return validationErrorResponse(result);
        }

        //creating a new Order if not already exists
        Order order = (orderId == null) ? new Order() : orderService.getOne(orderId);

        order.getOrderLines().add(orderLineRequestDto.toOrderLine());

        return ResponseEntity.ok().body(
                orderService.getOrderResponse(orderService.save(order))
        );
    }
}
