package org.bakhtiargalib.inventoryservice.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@Entity
public class Product extends Persistent {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "productIdGenerator", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "productIdGenerator", sequenceName = "product_seq", allocationSize = 1)
    private long id;

    @Size(max = 255)
    @NotNull
    @Column(length = 255, nullable = false)
    private String title;

    @Size(max = 3000)
    @Column(length = 3000)
    private String description;

    @Min(0)
    private double unitPrice;

    @Min(0)
    private int stockAmount;

    public Product update(Product product) {
        this.title = product.getTitle();
        this.description = product.getDescription();
        this.unitPrice = product.getUnitPrice();
        this.stockAmount = product.getStockAmount();

        return this;
    }
}
