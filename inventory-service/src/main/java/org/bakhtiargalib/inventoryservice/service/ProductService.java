package org.bakhtiargalib.inventoryservice.service;

import org.bakhtiargalib.inventoryservice.ResourceNotFoundException;
import org.bakhtiargalib.inventoryservice.dto.StockRemoveRequestDto;
import org.bakhtiargalib.inventoryservice.entity.Product;
import org.bakhtiargalib.inventoryservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> list(int index, int listSize, Sort sort) {

        return productRepository.findAll(PageRequest.of(index, listSize, sort)).toList();
    }

    public Product getOne(long id) {

        return productRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product with Id " + id + " not found"));
    }

    @Transactional
    public Product save(Product product) {

        return productRepository.save(product);
    }

    @Transactional
    public Product update(long id,
                          Product product) {

        return getOne(id).update(product);
    }

    @Transactional
    public Product removeStock(StockRemoveRequestDto stockRemoveRequest) {

        Product product = getOne(stockRemoveRequest.getProductId());

        if (product.getStockAmount() < stockRemoveRequest.getQuantity()) {
            throw new RuntimeException("Not enough stock for product " + stockRemoveRequest.getProductId());
        }

        product.setStockAmount(product.getStockAmount() - stockRemoveRequest.getQuantity());

        return product;
    }
}
