package org.bakhtiargalib.inventoryservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.bakhtiargalib.inventoryservice.dto.ProductRequestDto;
import org.bakhtiargalib.inventoryservice.dto.ProductResponseDto;
import org.bakhtiargalib.inventoryservice.dto.StockRemoveRequestDto;
import org.bakhtiargalib.inventoryservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.bakhtiargalib.inventoryservice.ApiResponseUtils.successResponse;
import static org.bakhtiargalib.inventoryservice.ApiResponseUtils.validationErrorResponse;

@Slf4j
@RestController
@RequestMapping("/products")
public class ProductController {

    private static final String DEFAULT_MAX_ITEMS_PER_PAGE = "10";
    private static final String DEFAULT_ORDER_BY_PROPERTY = "title";
    private static final String DEFAULT_ORDER_DIRECTION = "ASC";

    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<List<ProductResponseDto>> list(@RequestParam(defaultValue = "0") int index,
                                                         @RequestParam(defaultValue = DEFAULT_MAX_ITEMS_PER_PAGE) int listSize,
                                                         @RequestParam(defaultValue = DEFAULT_ORDER_BY_PROPERTY) String orderBy,
                                                         @RequestParam(defaultValue = DEFAULT_ORDER_DIRECTION) Sort.Direction orderDirection) {

        log.info("[ProductController:List] index = {}, " +
                        "listSize = {}, " +
                        "orderBy = {}, " +
                        "orderDirection = {}",
                index, listSize, orderBy, orderDirection);

        Sort sort = Sort.by(orderDirection, orderBy);

        return successResponse(
                productService.list(index, listSize, sort)
                        .stream()
                        .map(ProductResponseDto::new)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseDto> getOne(@PathVariable long id) {

        log.info("[ProductController:getOne] id = {}", id);

        return successResponse(
                new ProductResponseDto(productService.getOne(id))
        );
    }

    @PostMapping
    public ResponseEntity<?> save(@Valid @RequestBody ProductRequestDto productRequestDto,
                                  BindingResult result) {

        log.info("[ProductController:save] productRequestDto = {}", productRequestDto);

        if (result.hasErrors()) {

            return validationErrorResponse(result);
        }

        return successResponse(
                new ProductResponseDto(productService.save(productRequestDto.toProduct()))
        );
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable long id,
                                    @Valid @RequestBody ProductRequestDto productRequestDto,
                                    BindingResult result) {

        log.info("[ProductController:update] id = {}, productRequestDto = {}", id, productRequestDto);

        if (result.hasErrors()) {

            return validationErrorResponse(result);
        }

        return successResponse(
                new ProductResponseDto(productService.update(id, productRequestDto.toProduct()))
        );
    }

    @PutMapping("/removeStock")
    public ResponseEntity<?> removeStock(@Valid @RequestBody StockRemoveRequestDto stockRemoveRequest,
                                         BindingResult result) {

        log.info("[ProductController:removeStock] stockRemoveRequest = {}", stockRemoveRequest);

        if (result.hasErrors()) {

            return validationErrorResponse(result);
        }

        return successResponse(
                new ProductResponseDto(productService.removeStock(stockRemoveRequest))
        );
    }
}
