package org.bakhtiargalib.inventoryservice.controller;

import org.bakhtiargalib.inventoryservice.ErrorResponseDto;
import org.bakhtiargalib.inventoryservice.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.bakhtiargalib.inventoryservice.ApiResponseUtils.errorResponse;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException exception) {

        return errorResponse(
                new ErrorResponseDto("404", exception.getMessage(), null, null),
                HttpStatus.NOT_FOUND
        );
    }

    /*@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return validationErrorResponse(ex.getBindingResult());
    }*/
}
