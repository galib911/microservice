package org.bakhtiargalib.inventoryservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bakhtiargalib.inventoryservice.entity.Product;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ProductResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    private String title;

    private String description;

    private double unitPrice;

    private int stockAmount;

    public ProductResponseDto(Product product) {
        this.id = product.getId();
        this.title = product.getTitle();
        this.description = product.getDescription();
        this.unitPrice = product.getUnitPrice();
        this.stockAmount = product.getStockAmount();
    }
}
