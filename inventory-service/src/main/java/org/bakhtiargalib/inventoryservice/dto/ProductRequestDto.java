package org.bakhtiargalib.inventoryservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bakhtiargalib.inventoryservice.entity.Product;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ProductRequestDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Size(max = 255)
    @NotNull
    private String title;

    @Size(max = 3000)
    private String description;

    @Min(0)
    private double unitPrice;

    @Min(0)
    private int stockAmount;

    public Product toProduct() {

        return Product.builder()
                .title(title)
                .description(description)
                .unitPrice(unitPrice)
                .stockAmount(stockAmount)
                .build();
    }
}
