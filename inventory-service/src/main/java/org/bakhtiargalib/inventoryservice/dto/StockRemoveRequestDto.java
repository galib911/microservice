package org.bakhtiargalib.inventoryservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StockRemoveRequestDto {

    @NotNull
    private Long productId;

    @Min(1)
    @NotNull
    private Integer quantity;
}
