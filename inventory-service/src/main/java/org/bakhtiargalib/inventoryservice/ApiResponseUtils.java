package org.bakhtiargalib.inventoryservice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.stream.Collectors;

public class ApiResponseUtils {

    public static <T> ResponseEntity<T> successResponse(T responseBody) {

        return ResponseEntity.ok().body(
                responseBody
        );
    }

    public static ResponseEntity<ErrorResponseDto> errorResponse(ErrorResponseDto errorResponseDto) {

        return ResponseEntity.badRequest().body(
                errorResponseDto
        );
    }

    public static ResponseEntity<ErrorResponseDto> errorResponse(ErrorResponseDto errorResponseDto, HttpStatus statusCode) {

        return ResponseEntity.status(statusCode).body(errorResponseDto);
    }

    public static ResponseEntity<ErrorResponseDto> validationErrorResponse(BindingResult result) {

        return errorResponse(
                new ErrorResponseDto("201",
                        "Failed Validating Object",
                        result.getGlobalErrors()
                                .stream()
                                .map(o -> o.getDefaultMessage())
                                .collect(Collectors.toList()),
                        result.getFieldErrors()
                                .stream()
                                .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage))
                )
        );
    }
}
